# Introduction

<p>Robot Cleaner is a console application that emulates the operations of a cleaning robot that moves on a 2-D surface composed by 40 billion pieces.</p>

# The architecture

<p>The software is a single project application that stores the 2-D coordinates of the cleaned elements on a localdb after every move. The possible operations have been modeled by an implementation of the command pattern, for the DB access I chose to leverage a simple repository pattern on top of Entity Framework Core.</p>

<p> The solution uses standard I/O to get the user commands and rely on a multitasking approach to process multiple commands in parallel. A pessimistic concurrency model has been provided, probably the overall performance could be improved by moving part of this logic to a stored procedure.<p>

Some of the architectural choices I would like to mention:
 - Interface segregation
 - Repository pattern
 - SOLID approach with IoC
 - Observer pattern with multiple consumer tasks
 - Public interfaces and internal implementations to avoid coupling

Unit tests have been provided and are available on the codebase.
