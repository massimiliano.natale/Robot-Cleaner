﻿using Microsoft.EntityFrameworkCore;
using RobotCleaner.ConsoleApp.Entities;

namespace RobotCleaner.ConsoleApp.DataAccess
{
    /// <summary>
    /// DB Context pointed to a localdb instance.
    /// </summary>
    public class Context : DbContext
    {
        public DbSet<Point> Points { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=RobotCleanerDB;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Point>().HasKey(table => new
            {
                table.X,
                table.Y
            });
        }
    }
}
