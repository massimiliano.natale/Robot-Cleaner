﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RobotCleaner.ConsoleApp.Entities;

namespace RobotCleaner.ConsoleApp.DataAccess
{
    /// <summary>
    /// Repository used to write explored points on the database.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Get the number of cleaned points.
        /// </summary>
        /// <returns>Number of cleaned points.</returns>
        Task<long> GetNumberOfPointsAsync();

        /// <summary>
        /// Insert new explored points on the database.
        /// </summary>
        /// <param name="points">Explored points.</param>
        void Add(IEnumerable<Point> points);
    }

    internal class Repository : IRepository
    {
        private readonly object _lock = new object();

        public Repository()
        {
            using var context = new Context();

            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }

        public async Task<long> GetNumberOfPointsAsync()
        {
            await using var context = new Context();

            return await context.Points.LongCountAsync();
        }

        public void Add(IEnumerable<Point> points)
        {
            // Pessimistic concurrency model
            // Performance can be improved by doing this insert in atomic way
            // E.g. moving this logic in a stored procedure would help to avoid the lock
            lock (_lock)
            {
                var context = new Context();

                context.AddRange(points.Where(x => !context.Points.Contains(x)));
                context.SaveChanges();
            }
        }
    }
}
