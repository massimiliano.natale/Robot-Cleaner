﻿namespace RobotCleaner.ConsoleApp.Entities
{
    /// <summary>
    /// Robot cleaner. It moves on a square surface doing horizontal or vertical moves.
    /// </summary>
    public interface IRobotCleaner
    {
        int CurrentX { get; }
        int CurrentY { get; }
        void MoveToPosition(int x, int y);
    }

    internal class RobotCleaner : IRobotCleaner
    {
        public int CurrentX { get; private set; }
        public int CurrentY { get; private set; }

        public RobotCleaner(int initialX, int initialY)
        {
            CurrentX = initialX;
            CurrentY = initialY;
        }

        public void MoveToPosition(int x, int y)
        {
            CurrentX = x;
            CurrentY = y;
        }
    }
}
