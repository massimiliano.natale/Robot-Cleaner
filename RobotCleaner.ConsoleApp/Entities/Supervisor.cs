﻿using System;
using System.Threading.Tasks;
using RobotCleaner.ConsoleApp.Commands;
using RobotCleaner.ConsoleApp.DataAccess;
using RobotCleaner.ConsoleApp.Utilities;

namespace RobotCleaner.ConsoleApp.Entities
{
    /// <summary>
    /// Supervisor. It checks and tracks all the moves done by the robot cleaner.
    /// </summary>
    internal class Supervisor : IObserver<ICommand>
    {
        private readonly IRepository _repository;

        public Supervisor(IRepository repository)
        {
            _repository = repository;
        }

        public void OnNext(ICommand command)
        {
            var movement = command.Execute();

            AddCleanedPoints(movement);
        }

        public void OnCompleted() { }

        public void OnError(Exception error)
        {
            throw error;
        }

        public Task<long> GetNumberOfCleanedPointsAsync() => _repository.GetNumberOfPointsAsync();

        private void AddCleanedPoints((int startingX, int startingY, int endingX, int endingY) movement)
        {
            var cleanedPoints = PointHelper.GeneratePoints(movement.startingX, movement.startingY, movement.endingX, movement.endingY);

            _repository.Add(cleanedPoints);
        }
    }
}
