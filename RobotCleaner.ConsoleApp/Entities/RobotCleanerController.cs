﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using RobotCleaner.ConsoleApp.Commands;
using RobotCleaner.ConsoleApp.Utilities;

namespace RobotCleaner.ConsoleApp.Entities
{
    /// <summary>
    /// Robot cleaner controller. It turns on/off the robot cleaner and sends the commands put by the user.
    /// </summary>
    internal class RobotCleanerController : IObservable<ICommand>, IDisposable
    {
        private static IConsoleWrapper _consoleWrapper;
        private Dictionary<string, Func<int, ICommand>> _availableCommands;
        private IObserver<ICommand> _observer;

        public RobotCleanerController(IConsoleWrapper consoleWrapper)
        {
            _consoleWrapper = consoleWrapper;
        }

        public IDisposable Subscribe(IObserver<ICommand> observer)
        {
            _observer = observer;

            return this;
        }

        public int ReadNumberOfCommands() => Convert.ToInt32(_consoleWrapper.ReadLine());

        public (int x, int y) ReadStartingCoordinates()
        {
            var line = _consoleWrapper.ReadLine();
            Debug.Assert(line != null, nameof(line) + " != null");

            var segments = line.Split(" ");

            return (Convert.ToInt32(segments[0]), Convert.ToInt32(segments[1]));
        }

        public void ReadCommand()
        {
            var line = _consoleWrapper.ReadLine();
            Debug.Assert(line != null, nameof(line) + " != null");

            var segments = line.Split(" ");
            var commandKey = segments[0];
            var offset = Convert.ToInt32(segments[1]);

            _observer.OnNext(_availableCommands[commandKey].Invoke(offset));
        }

        public void TurnOnRobotCleaner(IRobotCleaner robotCleaner)
        {
            _availableCommands = new Dictionary<string, Func<int, ICommand>>
            {
                {"E", offset => new MoveToEastCommand(offset, robotCleaner)},
                {"W", offset => new MoveToWestCommand(offset, robotCleaner)},
                {"S", offset => new MoveToSouthCommand(offset, robotCleaner)},
                {"N", offset => new MoveToNorthCommand(offset, robotCleaner)}
            };
        }

        public async Task TurnOffRobotCleaner()
        {
            var supervisor = (Supervisor)_observer;
            var numberOfCleanedPoints = await supervisor.GetNumberOfCleanedPointsAsync();

            _consoleWrapper.WriteLine($"=> Cleaned: {numberOfCleanedPoints}");

            _observer.OnCompleted();
            _observer = null;
        }

        public IDisposable NotifySupervisor(IObserver<ICommand> observer) => Subscribe(observer);

        public void Dispose() { }
    }
}
