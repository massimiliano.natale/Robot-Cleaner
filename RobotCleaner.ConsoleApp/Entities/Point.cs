﻿namespace RobotCleaner.ConsoleApp.Entities
{
    /// <summary>
    /// Point entity. Used as an Entity Data Model.
    /// </summary>
    public class Point
    {
        public readonly int X;
        public readonly int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var point = (Point)obj;

            return point.X == X && point.Y == Y;
        }

        public override int GetHashCode() => new { X, Y }.GetHashCode();
    }
}
