﻿using RobotCleaner.ConsoleApp.Entities;

namespace RobotCleaner.ConsoleApp.Commands
{
    internal class MoveToNorthCommand : ICommand
    {
        public int Offset { get; }
        private readonly IRobotCleaner _robotCleaner;

        public MoveToNorthCommand(int offset, IRobotCleaner robotCleaner)
        {
            Offset = offset;
            _robotCleaner = robotCleaner;
        }

        public (int startingX, int startingY, int endingX, int endingY) Execute()
        {
            var startingX = _robotCleaner.CurrentX;
            var startingY = _robotCleaner.CurrentY;

            _robotCleaner.MoveToPosition(_robotCleaner.CurrentX, _robotCleaner.CurrentY + Offset);

            return (startingX, startingY, _robotCleaner.CurrentX, _robotCleaner.CurrentY);
        }
    }
}
