﻿namespace RobotCleaner.ConsoleApp.Commands
{
    public interface ICommand
    {
        int Offset { get; }
        (int startingX, int startingY, int endingX, int endingY) Execute();
    }
}
