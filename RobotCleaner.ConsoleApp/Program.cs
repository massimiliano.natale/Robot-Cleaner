﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using RobotCleaner.ConsoleApp.DataAccess;
using RobotCleaner.ConsoleApp.Entities;
using RobotCleaner.ConsoleApp.Utilities;

[assembly: InternalsVisibleTo("RobotCleaner.ConsoleApp.UnitTests")]
namespace RobotCleaner.ConsoleApp
{
    internal class Program
    {
        private static async Task Main()
        {
            var robotCleanerController = new RobotCleanerController(new ConsoleWrapper());

            var numberOfCommands = robotCleanerController.ReadNumberOfCommands();
            var (x, y) = robotCleanerController.ReadStartingCoordinates();

            robotCleanerController.TurnOnRobotCleaner(new Entities.RobotCleaner(x, y));
            robotCleanerController.NotifySupervisor(new Supervisor(new Repository()));

            var enqueuedCommands = new List<Task>();
            for (var i = 0; i < numberOfCommands; i++)
                enqueuedCommands.Add(Task.Factory.StartNew(() => robotCleanerController.ReadCommand()));

            Task.WaitAll(enqueuedCommands.ToArray());

            await robotCleanerController.TurnOffRobotCleaner();
        }
    }
}
