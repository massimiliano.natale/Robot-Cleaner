﻿using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace RobotCleaner.ConsoleApp.Utilities
{
    /// <summary>
    /// This is only a wrapper class over Console.ReadLine/WriteLine.
    ///
    /// Useful to make IOManager testable.
    /// </summary>
    internal interface IConsoleWrapper
    {
        string ReadLine();
        void WriteLine(string str);
    }

    internal class ConsoleWrapper : IConsoleWrapper
    {
        public string ReadLine() => Console.ReadLine();

        public void WriteLine(string str) => Console.WriteLine(str);
    }
}
