﻿using System;
using System.Collections.Generic;
using RobotCleaner.ConsoleApp.Entities;

namespace RobotCleaner.ConsoleApp.Utilities
{
    public static class PointHelper
    {
        /// <summary>
        /// Used to generated all points between 2 coordinates
        /// </summary>
        /// <param name="startingX">Starting X</param>
        /// <param name="startingY">Starting Y</param>
        /// <param name="endingX">Ending X</param>
        /// <param name="endingY">Ending Y</param>
        /// <returns>All points between the 2 coordinates</returns>
        public static IEnumerable<Point> GeneratePoints(int startingX, int startingY, int endingX, int endingY)
        {
            var points = new List<Point>();
            var minX = Math.Min(startingX, endingX);
            var maxX = Math.Max(startingX, endingX);
            var minY = Math.Min(startingY, endingY);
            var maxY = Math.Max(startingY, endingY);

            for (var i = minX; i <= maxX; i++)
                for (var j = minY; j <= maxY; j++)
                    points.Add(new Point(i, j));

            return points;
        }
    }
}
