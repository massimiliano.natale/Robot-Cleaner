﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using RobotCleaner.ConsoleApp.Commands;
using RobotCleaner.ConsoleApp.Entities;

namespace RobotCleaner.ConsoleApp.UnitTests.Commands
{
    [TestFixture]
    public class MoveToSouthCommandTests
    {
        private MoveToSouthCommand _target;
        private Mock<IRobotCleaner> _robotCleaner;

        [SetUp]
        public void SetUp()
        {
            _robotCleaner = new Mock<IRobotCleaner>();
            _target = new MoveToSouthCommand(1, _robotCleaner.Object);
        }

        [Test]
        public void MoveToSouthCommand_ShouldInitializeCorrectFields()
        {
            _target.Offset.Should().Be(1);
        }

        [Test]
        public void Execute_ShouldCallRobotCleanerAndReturnCoordinates()
        {
            _robotCleaner.SetupGet(robotCleaner => robotCleaner.CurrentX)
                .Returns(1);
            _robotCleaner.SetupGet(robotCleaner => robotCleaner.CurrentY)
                .Returns(1);

            var result = _target.Execute();

            _robotCleaner.Verify(x => x.MoveToPosition(1, 0), Times.Once);
            result.Should().Be((1, 1, 1, 1));
        }
    }
}
