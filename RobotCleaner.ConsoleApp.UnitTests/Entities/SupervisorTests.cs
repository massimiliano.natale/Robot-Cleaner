﻿using System.ComponentModel;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using RobotCleaner.ConsoleApp.Commands;
using RobotCleaner.ConsoleApp.DataAccess;
using RobotCleaner.ConsoleApp.Entities;
using RobotCleaner.ConsoleApp.Utilities;

namespace RobotCleaner.ConsoleApp.UnitTests.Entities
{
    [TestFixture]
    public class SupervisorTests
    {
        private Supervisor _target;
        private Mock<IRepository> _repository;

        [SetUp]
        public void SetUp()
        {
            _repository = new Mock<IRepository>();
            _target = new Supervisor(_repository.Object);
        }

        [TestCase(-10, 0, 10, 0)]
        [TestCase(-5, 22, -5, -13)]
        [TestCase(22, -4, -10, -4)]
        [TestCase(-10, -10, -10, -10)]
        [TestCase(-11, -23, -11, 55)]
        [TestCase(-11, -23, -11, 55)]
        public void OnNext_ShouldCallRepositoryWithCleanedPoints(int startingX, int startingY, int endingX, int endingY)
        {
            var command = new Mock<ICommand>();
            var cleanedPoints = PointHelper.GeneratePoints(startingX, startingY, endingX, endingY);

            command.Setup(x => x.Execute())
                .Returns((startingX, startingY, endingX, endingY));

            _target.OnNext(command.Object);

            command.Verify(x => x.Execute(), Times.Once);
            _repository.Verify(x => x.Add(cleanedPoints), Times.Once);
        }

        [Test]
        public void OnError_ShouldRethrowError()
        {
            Assert.Throws<InvalidAsynchronousStateException>(() => _target.OnError(new InvalidAsynchronousStateException()));
        }

        [Test]
        public async Task GetNumberOfCleanedPointsAsync_ShouldReturnNumberOfCleanedPoints()
        {
            _repository.Setup(x => x.GetNumberOfPointsAsync())
                .ReturnsAsync(41);

            var result = await _target.GetNumberOfCleanedPointsAsync();

            result.Should().Be(41);
        }
    }
}
