﻿using FluentAssertions;
using NUnit.Framework;

namespace RobotCleaner.ConsoleApp.UnitTests.Entities
{
    [TestFixture]
    public class RobotCleanerTests
    {
        private ConsoleApp.Entities.RobotCleaner _target;

        [SetUp]
        public void SetUp()
        {
            _target = new ConsoleApp.Entities.RobotCleaner(1, 1);
        }

        [Test]
        public void RobotCleaner_ShouldInitializeCorrectFields()
        {
            _target.CurrentX.Should().Be(1);
            _target.CurrentY.Should().Be(1);
        }

        [Test]
        public void MoveToPosition_ShouldMoveToCorrectPosition()
        {
            _target.MoveToPosition(2, 2);

            _target.CurrentX.Should().Be(2);
            _target.CurrentY.Should().Be(2);
        }
    }
}
