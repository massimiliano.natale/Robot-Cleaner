﻿using FluentAssertions;
using NUnit.Framework;
using RobotCleaner.ConsoleApp.Entities;

namespace RobotCleaner.ConsoleApp.UnitTests.Entities
{
    [TestFixture]
    public class PointTests
    {
        private Point _target;

        [SetUp]
        public void SetUp()
        {
            _target = new Point(5, 6);
        }

        [Test]
        public void Point_ShouldInitializeCorrectFields()
        {
            _target.X.Should().Be(5);
            _target.Y.Should().Be(6);
        }

        [TestCase(5, 5, false)]
        [TestCase(6, 6, false)]
        [TestCase(5, 6, true)]
        public void Equals_ShouldCorrectlyCompare(int newPointX, int newPointY, bool expected)
        {
            _target.Equals(new Point(newPointX, newPointY)).Should().Be(expected);
        }

        [Test]
        public void Equals_ShouldCorrectlyCompareWithNullValue()
        {
            _target.Equals(null).Should().Be(false);
        }
    }
}
