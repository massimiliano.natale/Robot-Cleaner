﻿using System.Collections.Generic;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using RobotCleaner.ConsoleApp.DataAccess;
using RobotCleaner.ConsoleApp.Entities;
using RobotCleaner.ConsoleApp.Utilities;

namespace RobotCleaner.ConsoleApp.UnitTests.Entities
{
    [TestFixture]
    public class RobotCleanerControllerTests
    {
        private RobotCleanerController _target;
        private Mock<IConsoleWrapper> _consoleWrapper;

        [SetUp]
        public void SetUp()
        {
            _consoleWrapper = new Mock<IConsoleWrapper>();
            _target = new RobotCleanerController(_consoleWrapper.Object);
        }

        [Test]
        public void ReadNumberOfCommands_ShouldConvertUserInput()
        {
            _consoleWrapper.Setup(x => x.ReadLine()).Returns("35");

            _target.ReadNumberOfCommands().Should().Be(35);
        }

        [Test]
        public void ReadStartingCoordinates_ShouldConvertUserInput()
        {
            _consoleWrapper.Setup(x => x.ReadLine()).Returns("14 15");

            _target.ReadStartingCoordinates().Should().Be((14, 15));
        }

        [TestCase("N", false)]
        [TestCase("S", false)]
        [TestCase("W", false)]
        [TestCase("E", false)]
        [TestCase("n", true)]
        [TestCase("s", true)]
        [TestCase("w", true)]
        [TestCase("e", true)]
        public void ReadCommand_ShouldConvertUserInput(string command, bool isException)
        {
            _consoleWrapper.Setup(x => x.ReadLine()).Returns($"{command} 10");
            _target.TurnOnRobotCleaner(Mock.Of<IRobotCleaner>());
            _target.NotifySupervisor(new Supervisor(Mock.Of<IRepository>()));

            if (isException)
                Assert.Throws<KeyNotFoundException>(() => _target.ReadCommand());
            else
                _target.ReadCommand();
        }
    }
}
