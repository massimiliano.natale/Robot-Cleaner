﻿using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using RobotCleaner.ConsoleApp.Utilities;

namespace RobotCleaner.ConsoleApp.UnitTests.Utilities
{
    [TestFixture]
    public class PointHelperTests
    {
        [TestCase(0, 0, 5, 5, 36, 0, 0, 5, 5)]
        [TestCase(5, 5, -5, -5, 121, -5, -5, 5, 5)]
        [TestCase(-6, -7, 6, 7, 195, -6, -7, 6, 7)]
        [TestCase(8, 9, -4, -3, 169, -4, -3, 8, 9)]
        public void GeneratePoints_ShouldCreateCorrectPoints(int startingX, int startingY, int endingX, int endingY, int totalNumber, int minX, int minY, int maxX, int maxY)
        {
            var points = PointHelper.GeneratePoints(startingX, startingY, endingX, endingY).ToList();

            points.Distinct().Count().Should().Be(totalNumber);
            points.Select(point => point.X).Distinct().Count().Should().Be(maxX - minX + 1);
            points.Select(point => point.Y).Distinct().Count().Should().Be(maxY - minY + 1);
            points.Select(point => point.X).OrderBy(x => x).First().Should().Be(minX);
            points.Select(point => point.Y).OrderBy(y => y).First().Should().Be(minY);
            points.Select(point => point.X).OrderByDescending(x => x).First().Should().Be(maxX);
            points.Select(point => point.Y).OrderByDescending(y => y).First().Should().Be(maxY);
        }
    }
}
